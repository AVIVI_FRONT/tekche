<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
</head>
<body>
<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="0" height="0" style="position:absolute">
    <symbol id="datepicker-icon"  viewBox="0 0 1000 1000" >
 <path d="M871.7,990H128.3c-37.3,0-67.6-30.3-67.6-67.6V179c0-37.3,30.3-67.6,67.6-67.6h67.6v33.8c0,56,45.4,101.4,101.4,101.4c56,0,101.4-45.4,101.4-101.4v-33.8h202.8v33.8c0,56,45.4,101.4,101.4,101.4c56,0,101.4-45.4,101.4-101.4v-33.8h67.6c37.3,0,67.6,30.3,67.6,67.6v743.5C939.3,959.7,909.1,990,871.7,990z M871.7,347.9H128.3v574.5h743.5L871.7,347.9L871.7,347.9z M297.2,516.9H195.9V415.5h101.4V516.9z M297.2,685.9H195.9V584.5h101.4V685.9L297.2,685.9z M297.2,854.8H195.9V753.5h101.4V854.8L297.2,854.8z M466.2,516.9H364.8V415.5h101.4V516.9z M466.2,685.9H364.8V584.5h101.4V685.9L466.2,685.9z M466.2,854.8H364.8V753.5h101.4V854.8L466.2,854.8z M635.2,516.9H533.8V415.5h101.4V516.9z M635.2,685.9H533.8V584.5h101.4V685.9L635.2,685.9z M635.2,854.8H533.8V753.5h101.4V854.8L635.2,854.8z M804.1,516.9H702.8V415.5h101.4V516.9z M804.1,685.9H702.8V584.5h101.4V685.9L804.1,685.9z M804.1,854.8H702.8V753.5h101.4V854.8L804.1,854.8z M701.7,212.8c-36.7,0-66.5-29.8-66.5-66.5V76.5c0-36.7,29.8-66.5,66.5-66.5c36.7,0,66.5,29.8,66.5,66.5v69.7C768.2,183,738.4,212.8,701.7,212.8z M296.2,212.8c-36.7,0-66.5-29.8-66.5-66.5V76.5c0-36.7,29.8-66.5,66.5-66.5c36.7,0,66.5,29.8,66.5,66.5v69.7C362.7,183,332.9,212.8,296.2,212.8z"/>
</symbol>
    <symbol  viewBox="0 0 1280.000000 1141.000000" id="arrow">
        <g transform="translate(0.000000,1141.000000) scale(0.100000,-0.100000)" >
            <path d="M6959 11387 c-24 -12 -1005 -852 -2179 -1867 -1174 -1014 -2624 -2267 -3222 -2783 -1182 -1021 -1128 -969 -1112 -1061 5 -29 19 -53 53 -87 41 -42 1245 -1069 5194 -4429 676 -575 1241 -1051 1255 -1059 62 -33 150 -9 194 51 l23 33 5 1310 5 1310 2030 5 2030 5 33 23 c18 13 40 38 50 55 16 30 17 192 17 2802 0 2752 0 2770 -20 2804 -11 19 -33 43 -48 55 l-28 21 -2032 5 -2032 5 -5 1350 c-5 1229 -6 1353 -21 1380 -22 39 -46 60 -93 79 -48 20 -44 20 -97 -7z"/>
        </g>
    </symbol>
    <symbol  viewBox="0 0 1280.000000 1141.000000" id="arrow-next">
        <g transform="translate(1280,1141) scale(-0.1,-0.100000)" >
            <path d="M6959 11387 c-24 -12 -1005 -852 -2179 -1867 -1174 -1014 -2624 -2267 -3222 -2783 -1182 -1021 -1128 -969 -1112 -1061 5 -29 19 -53 53 -87 41 -42 1245 -1069 5194 -4429 676 -575 1241 -1051 1255 -1059 62 -33 150 -9 194 51 l23 33 5 1310 5 1310 2030 5 2030 5 33 23 c18 13 40 38 50 55 16 30 17 192 17 2802 0 2752 0 2770 -20 2804 -11 19 -33 43 -48 55 l-28 21 -2032 5 -2032 5 -5 1350 c-5 1229 -6 1353 -21 1380 -22 39 -46 60 -93 79 -48 20 -44 20 -97 -7z"/>
        </g>
    </symbol>
    <symbol viewBox="0 0 505.78 845.2" id="scroll-next">
        <polygon  points="-0,0 170.35,0.06 505.78,422.6 170.35,845.2 -0,845.2 336.76,422.6 "/>
        <polygon  points="-0,169.04 170.35,422.6 -0,677.48 "/>
    </symbol>
</svg>
<?php $months = array(1 => 'Jan.', 2 => 'Feb.', 3 => 'Mar.', 4 => 'Apr.', 5 => 'May', 6 => 'Jun.', 7 => 'Jul.', 8 => 'Aug.', 9 => 'Sep.', 10 => 'Oct.', 11 => 'Nov.', 12 => 'Dec.');
    $colors = array(
        array( '#F2D7D5', '#922B21' ),
        array( '#FADBD8', '#B03A2E' ),
        array( '#EBDEF0', '#76448A' ),
        array( '#E8DAEF', '#6C3483' ),
        array( '#D4E6F1', '#1F618D' ),
        array( '#AED6F1', '#2874A6' ),
        array( '#D1F2EB', '#148F77' ),
        array( '#A2D9CE', '#117A65' ),
        array( '#7DCEA0', '#1E8449' ),
        array( '#D5F5E3', '#239B56' ),
        array( '#FCF3CF', '#B7950B' ),
        array( '#FAD7A0', '#B9770E' ),
        array( '#F8C471', '#9C640C' ),
        array( '#F5CBA7', '#AF601A' ),
        array( '#EF5350', '#B71C1C' ),
        array( '#F48FB1', '#C2185B' ),
        array( '#CE93D8', '#6A1B9A' ),
        array( '#7986CB', '#1A237E' ),
        array( '#64B5F6', '#1565C0' ),
        array( '#00BCD4', '#006064' ),
    );
    function randColor($i, $colors) {
        $rand = array_rand($colors);
        echo $colors[$rand][$i];
    }

?>
<div class="app-teamreport" id="app-teamreport">
    <?php  include 'app-header.html';?>
    <div class="app-body">
        <div  class="carcas">
            <div class="carcas__users ">
                <button class="datepicker-button">
                    <svg><use xlink:href="#datepicker-icon"></use></svg>
                </button>
                <div class="users users--mini">
                    <?php for ($ra = 1 ; $ra < 17; $ra++):
                        include 'user.html';
                    endfor;?>
                </div>
            </div>
            <div data-rel="week" class="carcas__calendar active">
                <div class="calendar__outter">
                    <div class="calendar">
                        <div class="calendar__wrap">
                            <div class="calendar__header">
                                <div class="greed__row greed__row--small">
                                    <?php for ($j = 1 ; $j < 2; $j++): ?>
                                        <?php for ($i = 1 ; $i <= 7; $i++): ?>
                                            <div class="greed__cell  <?php
                                            if($i == 1 || ($i - 1) % 7 == 0){echo "new-week";}
                                            if($i  == 4 && $j == 1){echo "today";}
                                            if(($i + 1) % 7 == 0 || $i % 7 == 0){echo "weekend";} ?> " >
                                                <div class="calendar__day">
                                                    <?php if($i == 1 || ($i - 1) % 7 == 0):?>
                                                        <div class="week-number"><?= (($i - $i % 7) / 7) * $j ?></div>
                                                    <?php endif;?>
                                                    <div class="calendar__day-inner">
                                                        <div class="calendar__top">
                                                            <?php if($i == 1):?>
                                                                <div class="month-name"><?= $months[$j]?></div>
                                                            <?php endif;?>
                                                        </div>

                                                        <div class="calendar__date">  Monday <?= $i?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endfor; ?>
                                    <?php endfor; ?>
                                </div>
                            </div>
                            <div class="users__tasks">
                                <?php for ($re = 1 ; $re < 7; $re++): ?>
                                    <div class="greed__row greed__row--small">
                                        <?php for ($r = 1 ; $r < 8; $r++): ?>
                                            <div class="greed__cell">
                                                <div class="users__box">
                                                    <div class="users__task-emiter disabled"></div>
                                                    <div class="users__task-emiter disabled"></div>
                                                </div>
                                            </div>
                                        <?php endfor; ?>
                                        <div class="users__task" style=" color: <?php $i = rand(0,1);  echo $i > 0 ? '#fff' : '#000'; ?> ; width: 400px; transform: translate(200px, 0 ); background: <?php randColor($i, $colors)?>">
                                            <?php include 'task.html'?>
                                        </div>
                                        <div class="users__task" style="color: <?php $i = rand(0, 1);  echo $i > 0 ? '#fff' : '#000'; ?> ;width: 200px; transform: translate(600px, 27px ); background: <?php randColor($i, $colors)?>">
                                            <?php include 'task.html'?>
                                        </div>
                                    </div>
                                <?php endfor; ?>
                            </div>
                        </div>
                    </div>
                    <?php  include 'calendars-scroll.html';?>
                </div>
            </div>
            <div data-rel="day" class="carcas__calendar">
                <div class="calendar__outter">
                    <div class="calendar">
                        <div class="calendar__wrap">
                            <div class="calendar__header">
                                <div class="greed__row greed__row--big">
                                    <?php for ($j = 1 ; $j < 10; $j++): ?>
                                        <?php for ($i = 1 ; $i <= 28; $i++): ?>
                                            <div class="greed__cell <?php
                                            if($i == 1 || ($i - 1) % 7 == 0){echo "new-week";}
                                            if($i  == 4 && $j == 1){echo "today";}
                                            if(($i + 1) % 7 == 0 || $i % 7 == 0){echo "weekend";} ?> " >
                                                <div class="calendar__day">
                                                    <?php if($i == 1 || ($i - 1) % 7 == 0):?>
                                                        <div class="week-number"><?= (($i - $i % 7) / 7) * $j ?></div>
                                                    <?php endif;?>
                                                    <div class="calendar__day-inner">
                                                        <div class="calendar__top">
                                                            <?php if($i == 1):?>
                                                                <div class="month-name"><?= $months[$j]?></div>
                                                            <?php endif;?>
                                                        </div>

                                                        <div class="calendar__date">  <?= $i  ?>:00</div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endfor; ?>
                                    <?php endfor; ?>
                                </div>
                            </div>
                            <div class="users__tasks">
                                <div class="greed__row greed__row--big">
                                    <?php for ($r = 1 ; $r < 280; $r++): ?>
                                        <div class="greed__cell">
                                            <div class="users__box">
                                                <div class="users__task-emiter disabled"></div>
                                            </div>
                                        </div>
                                    <?php endfor; ?>
                                    <div class="users__task" style="width: 108px; transform: translate(1080px, 0 ); background: red;">
                                        <?php include 'task.html'?>
                                    </div>

                                </div>
                                <div class="greed__row greed__row--big">
                                    <?php for ($r = 1 ; $r < 280; $r++): ?>
                                        <div class="greed__cell">
                                            <div class="users__box">
                                                <div class="users__task-emiter disabled"></div>
                                            </div>
                                        </div>
                                    <?php endfor; ?>
                                    <div class="users__task" style="width: 108px; transform: translate(1080px, 0 ); background: red;">
                                        <?php include 'task.html'?>
                                    </div>
                                    <div class="users__task" style="width: 216px; transform: translate(540px, 0 ); background: green;">
                                        <?php include 'task.html'?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php  include 'calendars-scroll.html';?>
                </div>
            </div>
            <div data-rel="month" class="carcas__calendar">
                <div class="calendar__outter">
                    <div class="calendar">
                        <div class="calendar__wrap">

                            <div class="calendar__header">
                                <div class="greed__row">
                                    <?php for ($j = 1 ; $j < 10; $j++): ?>
                                        <?php for ($i = 1 ; $i <= 28; $i++): ?>
                                            <div class="greed__cell <?php
                                            if($i == 1 || ($i - 1) % 7 == 0){echo "new-week";}
                                            if($i  == 4 && $j == 1){echo "today";}
                                            if(($i + 1) % 7 == 0 || $i % 7 == 0){echo "weekend";} ?> " >
                                                <div class="calendar__day">
                                                    <?php if($i == 1 || ($i - 1) % 7 == 0):?>
                                                        <div class="week-number"><?= (($i - $i % 7) / 7) * $j ?></div>
                                                    <?php endif;?>
                                                    <div class="calendar__day-inner">
                                                        <div class="calendar__top">
                                                            <?php if($i == 1):?>
                                                                <div class="month-name"><?= $months[$j]?></div>
                                                            <?php endif;?>
                                                        </div>

                                                        <div class="calendar__date">  Mo <?= $i  ?></div>
                                                    </div>
                                                </div>
                                            </div>
                                        <?php endfor; ?>
                                    <?php endfor; ?>
                                </div>
                            </div>
                            <div class="users__tasks">
                                <div class="greed__row">
                                    <?php for ($r = 1 ; $r < 280; $r++): ?>
                                        <div class="greed__cell">
                                            <div class="users__box">
                                                <div class="users__task-emiter disabled"></div>
                                                <div class="users__task-emiter disabled"></div>
                                            </div>
                                        </div>
                                    <?php endfor; ?>
                                    <div class="users__task" style="width: 108px; transform: translate(1080px, 0 ); background: red;">
                                        <?php include 'task.html'?>
                                    </div>
                                    <div class="users__task" style="width: 216px; transform: translate(1080px, 27px ); background: green;">
                                        <?php include 'task.html'?>
                                    </div>
                                </div>
                                <div class="greed__row">
                                    <?php for ($r = 1 ; $r < 280; $r++): ?>
                                        <div class="greed__cell">
                                            <div class="users__box">
                                                <div class="users__task-emiter disabled"></div>
                                                <div class="users__task-emiter disabled"></div>
                                            </div>
                                        </div>
                                    <?php endfor; ?>
                                    <div class="users__task" style="width: 108px; transform: translate(1080px, 0 ); background: red;">
                                        <?php include 'task.html'?>
                                    </div>
                                    <div class="users__task" style="width: 216px; transform: translate(1080px, 27px ); background: green;">
                                        <?php include 'task.html'?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php  include 'calendars-scroll.html';?>
                </div>
            </div>
        </div>


    </div>
</div>
<link rel="stylesheet" href="css/plugins.css">
<link rel="stylesheet" href="css/dev.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script src="js/jquery.formstyler.min.js"></script>
<script>
    $(document).ready(function () {
        $('select.styler').styler({
            selectSmartPositioning:false,
            onFormStyled: function() {
                $('.app-dropdown').addClass('show');
            }});
        dataTitle();
        arrowsInit();
    });
    var dublicate = false;
    function duplicate() {
        if (!dublicate){
            dublicate = true;
            if(!$('.active .calendar__header').hasClass('duplicate')){
                var dub =  $('.active .calendar__header').clone();
                dub.addClass('duplicate').appendTo('.active .calendar__wrap');
            }
            dublicate = false;
        }

    }

    window.onscroll = function(){
        duplicate();
        if(window.scrollY > 120){
            $('.calendar__header.duplicate').addClass('fixed').css('top', window.scrollY - 100);
        }else{
            $('.calendar__header.duplicate').removeClass('fixed')
        }
    }
    $(document).on('click', function (event) {
       if(!$('.app-tooltip').is(event.target) && $('.app-tooltip').has(event.target).length === 0 && !$('.users__task').is(event.target) && $('.users__task').has(event.target).length === 0){
           $('.app-tooltip').remove();
       }
    });
    $(document).on('click', '.app-header__button', function (e) {
        $('.carcas__calendar').removeClass('active');
        $('.app-header__button').removeClass('active');
        $(this).addClass('active');
        $('[data-rel='+$(this).data('rel')+']').addClass('active');
        arrowChecker();
    });
    $(document).on('click', '.users__task', function (e) {
        var app = $('.app-teamreport');
        var appX = app[0].getBoundingClientRect().left;
        var appY = app[0].getBoundingClientRect().top;
        var thisX = this.getBoundingClientRect().left;
        var thisY = this.getBoundingClientRect().top;
        $('.app-tooltip').remove();
        var tooltip =  document.createElement('div');
        $(tooltip).html($(this).find('.users__dropdown').html()).addClass('app-tooltip');
        $(tooltip).css({
            'left': thisX - appX,
            'top': thisY - appY
        });
        app.append(tooltip);
    });
    function dataTitle() {
        var tips = '[data-title]';
        if ($(tips).length > 0){
            var app = $('.app-teamreport');
            var appX = app[0].getBoundingClientRect().left;
            var appY = app[0].getBoundingClientRect().top;
            $(document).on('mouseenter', tips, showTips);
            $(document).on('mousemove', tips, moveTips);
            $(document).on('mouseleave', tips, hideTips);
        }
        function moveTips(e) {
            $('.app-title').css({
                'left': e.pageX - appX,
                'top': e.pageY - appY
            });
        }
        function showTips(e){
            var text =  $(this).data('title');
            var tooltip =  document.createElement('div');
            $(tooltip).text(text).addClass('app-title');
            $(tooltip).css({
                'left': e.pageX - appX,
                'top': e.pageY - appY
            });
            app.append(tooltip);
        }
        function hideTips() {
           $('.app-title').remove();
        }
    }
    function arrowChecker(){
        var box = $('.carcas__calendar.active .calendar'),
            next = $('.carcas__calendar.active .calendar__next'),
            prev = $('.carcas__calendar.active .calendar__prev');
        prev.removeClass('hide');
        next.removeClass('hide');
        if(box[0].scrollLeft +  box.width() >= box[0].scrollWidth) next.addClass('hide');
        if(box[0].scrollLeft <= 0 ) prev.addClass('hide');
        if(box[0].scrollWidth <= box.width() ) {
            prev.addClass('hide');
            next.addClass('hide');
        }
    }
    function arrowsInit(){
        var scrollStream,
        speed = 20,
        interval = 30,
        scrollTimeout = null;

        arrowChecker();
        $(document).on('mouseenter', '.calendar__next', moveRight);
        $(document).on('mouseenter', '.calendar__prev', moveLeft);
        $(document).on('mouseleave', '.calendar__prev', stopScroll);
        $(document).on('mouseleave', '.calendar__next', stopScroll);
        $('.calendar').scroll(function(){
            if (scrollTimeout) clearTimeout(scrollTimeout);
            scrollTimeout = setTimeout(function(){arrowChecker()},500);
        });

        function stopScroll(e) {
            clearInterval(scrollStream);
            arrowChecker();
        }
        function moveRight(e){
            var box = $(this).closest('.calendar__outter').find('.calendar');
            scrollStream = setInterval(function () {
                box[0].scrollLeft += speed
            }, interval)
        }
        function moveLeft(e){
            var box = $(this).closest('.calendar__outter').find('.calendar');
           scrollStream = setInterval(function () {
               box[0].scrollLeft -= speed
            }, interval)
        }
    }
</script>
</body>
</html>